package gol.study.repository;

import gol.study.model.Car;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author Golovanov Egor (Egor.Golovanov@billing.ru)
 */
@Mapper
public interface CarsRepository {

    @Select("select * from cars")
    List<Car> getAllCars();
}
