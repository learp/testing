package gol.study.repository;

import gol.study.model.User;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * @author Golovanov Egor (Egor.Golovanov@billing.ru)
 */
@Mapper
public interface UserRepository {
    @Select("SELECT id, name, country, phone, email FROM users")
    List<User> getAllUsers();

    @Select("SELECT id, name, country, phone, email FROM users u WHERE u.id = #{id}")
    User getUser(long id);

    @Insert("INSERT INTO users(name, country, phone, email) VALUES (#{name}, #{country}, #{phone}, #{email})")
    void addUser(User user);

    @Update("UPDATE users SET name=#{user.name}, country=#{user.country}, phone=#{user.phone}, email=#{user.email} WHERE id=#{id}")
    void updateUser(@Param("id") long id, @Param("user") User user);

    @Delete("DELETE FROM users WHERE id=#{id}")
    void deleteUser(int id);
}
