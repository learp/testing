package gol.study.config;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;

import javax.sql.DataSource;

import static org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType.H2;


/**
 * Конфигурация для создания бинов репозиториев на основе MyBatis
 */
@Configuration
@MapperScan("gol.study.repository")
public class DbConfig {

    @Bean
    public DataSource dataSource() {
        return new EmbeddedDatabaseBuilder().
            setType(H2).
            continueOnError(false).
            addScript("db.sql").
            build();
    }

    @Bean
    public SqlSessionTemplate sqlSessionTemplate(SqlSessionFactory sqlSessionFactory) {
        return new SqlSessionTemplate(
            sqlSessionFactory,
            sqlSessionFactory.getConfiguration().getDefaultExecutorType());
    }
}
