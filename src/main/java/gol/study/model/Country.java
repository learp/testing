package gol.study.model;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Golovanov Egor (Egor.Golovanov@billing.ru)
 */
public enum Country {
    RUSSIA(Pattern.compile("\\+7")),
    USA(Pattern.compile("\\+1"));

    Country(Pattern pattern) {
        this.pattern = pattern;
    }

    boolean isNumberCompatible(String tel) {
        return true;
    }

    Pattern pattern;
}
