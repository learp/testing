package gol.study.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Setter;
import lombok.ToString;

/**
 * @author Golovanov Egor (Egor.Golovanov@billing.ru)
 */
@ToString
@JsonSerialize
@JsonDeserialize
public class User {

    public User(Long id, String name, String country, String email, String phone) {
        this.id = id;
        this.name = name;
        this.country = Country.valueOf(country);
        this.email = email;
        this.phone = phone;
    }

    @JsonCreator
    public User(
        @JsonProperty("name") String name,
        @JsonProperty("country") Country country,
        @JsonProperty("email") String email,
        @JsonProperty("phone") String phone) {

        this(null, name, country.name(), email, phone);
    }

    public Long id;
    public String name;
    public Country country;
    public String phone;
    public String email;
}
