package gol.study.http;

import gol.study.model.User;
import gol.study.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Golovanov Egor (Egor.Golovanov@billing.ru)
 */
@RestController
@RequestMapping(path = "/users")
public class UsersController {

    @GetMapping
    public List<User> getAllUsers() {
        return repository.getAllUsers();
    }

    @GetMapping(path = "{id}")
    public User getUser(@PathVariable int id) {
        return repository.getUser(id);
    }

    @PostMapping(consumes = "application/json;charset=UTF-8")
    public void addUser(@RequestBody User user) {
        repository.addUser(user);
    }

    @PutMapping(path = "{id}", consumes = "application/json;charset=UTF-8")
    public void updateUser(@PathVariable int id, @RequestBody User newUser) {
        User currentUser = repository.getUser(id);

        if (currentUser == null) {
            throw new IllegalArgumentException("User with id={0} does not exist");
        }

        repository.updateUser(id, newUser);
    }

    @DeleteMapping(path = "{id}",consumes = "application/json;charset=UTF-8")
    public void deleteUser(@PathVariable int id) {
        repository.deleteUser(id);
    }

    @Autowired
    UserRepository repository;
}
